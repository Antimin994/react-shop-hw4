export const clothes = (state) => state.clothes
export const accessories = (state) => state.accessories
export const selectFavorites = (state) => state.favorites
export const selectBaskets = (state) => state.baskets
export const selectCurrentItem = (state) => state.currentItem
export const selectApp = (state) => state
export const isModal = (state) => state.isModal
export const isModalImage = (state) => state.isModalImage
