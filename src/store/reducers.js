import {createReducer, current} from "@reduxjs/toolkit";
import * as actions from "./actions.js";

const initialState =  {
	favorites: JSON.parse(localStorage.getItem('favorite')) !== null ? JSON.parse(localStorage.getItem('favorite')) : [],
    baskets: JSON.parse(localStorage.getItem('basket')) !== null ? JSON.parse(localStorage.getItem('basket')) : [],
	currentItem: [],
	clothes: [],
	accessories: [],
	isModal: false,
	isModalImage: false
}

export default  createReducer(initialState, (builder)=>{
    builder
    .addCase(actions.handleFavorite, (state, action) => {
	  const isAdded = state.favorites.some((favorite)=> favorite.id === action.payload.id)
      
	if(isAdded){
		state.favorites = state.favorites.filter(i => i.id !== action.payload.id)
	}
	else {
		state.favorites = [ ...state.favorites, action.payload]
	}
	localStorage.setItem('favorite', JSON.stringify(state.favorites))
    })
	.addCase(actions.handleBasket, (state, action) => {
		state.currentItem = [];
		if (state.baskets.some((basket)=> basket.id === action.payload.id)) {
			
			state.currentItem = state.baskets.map((basket) => {
				console.log(basket.id === action.payload.id);
					switch (!basket.quantity) {
						case true:
						return basket.id === action.payload.id ? { ...basket, quantity: 2 } : basket
						case false:
						return basket.id === action.payload.id ? { ...basket, quantity: ++basket.quantity } : basket
					}							
			  })
		} else {
			state.currentItem = [...state.baskets,action.payload]
		}
	})
	.addCase(actions.handleConfirmed, (state, action) => {
		state.baskets = action.payload;
		localStorage.setItem('basket', JSON.stringify(state.baskets));
	})
	.addCase(actions.plusBasket, (state, action) => {
		state.baskets = state.baskets.map((basket) => {
			if (basket.id === action.payload.id) {
				switch (!basket.quantity) {
					case true:
					return basket.id === action.payload.id ? { ...basket, quantity: 2 } : basket
					case false:
					return basket.id === action.payload.id ? { ...basket, quantity: ++basket.quantity } : basket
				}							
			} else {
				return basket;
			}
		  })
		  localStorage.setItem('basket', JSON.stringify(state.baskets));
	 })
	 .addCase(actions.minusBasket, (state, action) => {
	
		console.log(state.baskets.some((basket)=> {basket.id === action.payload.id && (basket.quantity === undefined || basket.quantity <= 1)}));
	  if(state.baskets.some((basket)=> (basket.id === action.payload.id) && ((basket.quantity === undefined)||(basket.quantity <= 1)))){
		  state.baskets = state.baskets.filter(i => i.id !== action.payload.id)
	  }
	  else {
		state.baskets = state.baskets.map((basket) => {
			return basket.id === action.payload.id ? { ...basket, quantity: --basket.quantity } : basket
		})
	  }
	  localStorage.setItem('basket', JSON.stringify(state.baskets))
	  })
	  .addCase(actions.removeBasket, (state, action) => {
		state.currentItem = state.baskets.filter(i => i.id !== action.payload.id)
	  })
	  .addCase(actions.actionAddToClothes, (state, action) => {
		state.clothes = action.payload;
	  })
	  .addCase(actions.actionAddToAccessories, (state, action) => {
		state.accessories = action.payload;
	  })
	  .addCase(actions.handleModal, (state, action) => {
		console.log(!state.isModal);
		return !state.isModal;
	  })
	  .addCase(actions.handleModalImage, (state, action) => {
		console.log(!state.isModalImage);
		return !state.isModalImage;
	  })
	
	
})
